use denmanadjer::backend_api::create_app;

#[tokio::main]
async fn main() {
    let app = create_app();

    let address = std::net::SocketAddr::from((std::net::Ipv4Addr::UNSPECIFIED, 8080));

    let listener = tokio::net::TcpListener::bind(&address).await.unwrap();
    axum::serve(listener, app).await.unwrap();
}
