use axum::http::StatusCode;
use uuid::Uuid;

use crate::messages::ResponseItem;

#[utoipa::path(
        get,
        path = "/v0/den/items/{id}",
        responses(
            (status = 200, description = "Item retrieved ", body = ResponseItem),
            (status = NOT_FOUND, description = "failed find")
        ),
        params(
            ("id" = Uuid, Path, description = "Item UUID to get"),
        )
)]
pub async fn get_item_by_id(_item_id: Uuid) -> Result<(StatusCode, ResponseItem), StatusCode> {
    Err(StatusCode::INTERNAL_SERVER_ERROR)
}
