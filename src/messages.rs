use bigdecimal::BigDecimal;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use url::Url;
use utoipa::ToSchema;
use uuid::Uuid;

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct RequestCategory {
    #[schema(format = Ulid)]
    pub id: Option<Uuid>,
    pub notes: Vec<RequestNote>,
    pub name: String,
    pub sub_categories: Vec<RequestCategory>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct ResponseCategory {
    #[schema(format = Ulid)]
    pub id: Uuid,
    pub notes: Vec<ResponseNote>,
    pub name: String,
    pub sub_categories: Vec<ResponseCategory>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct RequestQuantity {
    #[schema(format = Ulid)]
    pub id: Option<Uuid>,
    pub name: String,
    pub unit: String,
    pub value: f64,
    #[schema(value_type = String, format = DateTime)]
    pub timestamp: Option<DateTime<Utc>>,
    pub notes: Vec<RequestNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct ResponseQuantity {
    #[schema(format = Ulid)]
    pub id: Uuid,
    pub name: String,
    pub unit: String,
    pub value: f64,
    #[schema(value_type = String, format = DateTime)]
    pub timestamp: Option<DateTime<Utc>>,
    pub notes: Vec<ResponseNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct RequestCost {
    #[schema(format = Ulid)]
    pub id: Option<Uuid>,
    pub expense: String,
    pub currency: String,
    pub value: BigDecimal,
    #[schema(value_type = String, format = DateTime)]
    pub timestamp: Option<DateTime<Utc>>,
    pub notes: Vec<RequestNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct ResponseCost {
    #[schema(format = Ulid)]
    pub id: Uuid,
    pub expense: String,
    pub currency: String,
    pub value: BigDecimal,
    #[schema(value_type = String, format = DateTime)]
    pub timestamp: Option<DateTime<Utc>>,
    pub notes: Vec<ResponseNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct RequestNote {
    #[schema(format = Ulid)]
    pub id: Option<Uuid>,
    pub text: String,
    #[schema(value_type = String, format = DateTime)]
    pub created_at: Option<DateTime<Utc>>,
    #[schema(value_type = String, format = DateTime)]
    pub updated_at: Option<DateTime<Utc>>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct ResponseNote {
    #[schema(format = Ulid)]
    pub id: Uuid,
    pub text: String,
    #[schema(value_type = String, format = DateTime)]
    pub created_at: DateTime<Utc>,
    #[schema(value_type = String, format = DateTime)]
    pub updated_at: Option<DateTime<Utc>>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct RequestFediverseInstance {
    #[schema(format = Ulid)]
    pub id: Option<Uuid>,
    #[schema(value_type = String, format = Uri)]
    pub instance: Url,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct ResponseFediverseInstance {
    #[schema(format = Ulid)]
    pub id: Uuid,
    #[schema(value_type = String, format = Uri)]
    pub instance: Url,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub enum RequestPlatform {
    Reddit,
    Mastodon(RequestFediverseInstance),
    Lemmy(RequestFediverseInstance),
    PixlFed(RequestFediverseInstance),
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub enum ResponsePlatform {
    Reddit,
    Mastodon(ResponseFediverseInstance),
    Lemmy(ResponseFediverseInstance),
    PixlFed(ResponseFediverseInstance),
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct RequestPersonHandle {
    #[schema(format = Ulid)]
    pub id: Option<Uuid>,
    pub platform: RequestPlatform,
    pub instance: Option<String>,
    pub notes: Vec<RequestNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct ResponsePersonHandle {
    #[schema(format = Ulid)]
    pub id: Uuid,
    pub platform: ResponsePlatform,
    pub instance: Option<String>,
    pub notes: Vec<ResponseNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct RequestPerson {
    #[schema(format = Ulid)]
    pub id: Option<Uuid>,
    pub name: String,
    pub handles: Vec<RequestPersonHandle>,
    pub notes: Vec<RequestNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct ResponsePerson {
    #[schema(format = Ulid)]
    pub id: Uuid,
    pub name: String,
    pub handles: Vec<ResponsePersonHandle>,
    pub notes: Vec<ResponseNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub enum IdOr<T> {
    Id(Uuid),
    Val(T),
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct RequestOwnershipStatus {
    #[schema(format = Ulid)]
    pub id: Option<Uuid>,
    pub owner: Option<IdOr<RequestPerson>>,
    pub possessor: Option<IdOr<RequestPerson>>,
    #[schema(value_type = String, format = DateTime)]
    pub start_date: Option<DateTime<Utc>>,
    #[schema(value_type = String, format = DateTime)]
    pub end_date: Option<DateTime<Utc>>,
    pub previous: Option<Box<RequestOwnershipStatus>>,
    pub notes: Vec<RequestNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct ResponseOwnershipStatus {
    #[schema(format = Ulid)]
    pub id: Uuid,
    pub owner: Option<ResponsePerson>,
    pub possessor: Option<ResponsePerson>,
    #[schema(value_type = String, format = DateTime)]
    pub start_date: DateTime<Utc>,
    #[schema(value_type = String, format = DateTime)]
    pub end_date: Option<DateTime<Utc>>,
    pub previous: Option<Box<ResponseOwnershipStatus>>,
    pub notes: Vec<ResponseNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct RequestNamed {
    #[schema(format = Ulid)]
    pub id: Option<Uuid>,
    pub name: String,
    pub notes: Vec<RequestNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct ResponseNamed {
    #[schema(format = Ulid)]
    pub id: Uuid,
    pub name: String,
    pub notes: Vec<ResponseNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct RequestItem {
    #[schema(format = Ulid)]
    pub id: Option<Uuid>,
    pub brands: Vec<IdOr<RequestNamed>>,
    pub model: RequestNamed,
    pub name: RequestNamed,
    pub aliases: Vec<RequestNamed>,
    pub ownership: RequestOwnershipStatus,
    pub categories: Vec<IdOr<RequestCategory>>,
    pub quantities: Vec<IdOr<RequestQuantity>>,
    pub notes: Vec<RequestNote>,
}

#[derive(Clone, Debug, Deserialize, Serialize, ToSchema)]
pub struct ResponseItem {
    #[schema(format = Ulid)]
    pub id: Uuid,
    #[schema(inline)]
    pub brands: Vec<ResponseNamed>,
    #[schema(inline)]
    pub model: ResponseNamed,
    #[schema(inline)]
    pub name: ResponseNamed,
    #[schema(inline)]
    pub aliases: Vec<ResponseNamed>,
    #[schema(inline)]
    pub ownership: ResponseOwnershipStatus,
    #[schema(inline)]
    pub categories: Vec<ResponseCategory>,
    #[schema(inline)]
    pub quantities: Vec<ResponseQuantity>,
    #[schema(inline)]
    pub notes: Vec<ResponseNote>,
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use serde_json::json;

    use super::*;

    #[test]
    fn test_request_note() {
        let id = Uuid::now_v7();
        let text = "Affen mit Ohren und paffen mit Poren";
        let creation_time = Utc::now();
        let json_value = json!({
            "id": id,
            "text": text,
            "created_at":creation_time,
        });
        let note: RequestNote = serde_json::from_value(json_value).unwrap();
        assert_eq!(note.id, Some(id));
        assert_eq!(note.text, text);
        assert_eq!(note.created_at, Some(creation_time));
    }

    #[test]
    fn test_request_cost() {
        let expense = "Purchase price at Maggard";
        let currency = "USD";
        let value_str = "99.42";
        let ts_str = "2023-12-07T22:45:19.077+01:00";
        let json_value = json!({
            "expense": expense,
            "currency": currency,
            "value": value_str,
            "timestamp": ts_str,
            "notes": [],
        });
        let cost: RequestCost = serde_json::from_value(json_value).unwrap();

        assert_eq!(expense, cost.expense);
        assert_eq!(currency, cost.currency);
        assert_eq!(BigDecimal::from_str(value_str).unwrap(), cost.value);
        assert_eq!(Some(DateTime::from_str(ts_str).unwrap()), cost.timestamp);
    }
}
