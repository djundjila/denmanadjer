use axum::Router;
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

use crate::backend_handlers;

use crate::messages::*;

#[derive(OpenApi)]
#[openapi(
    paths(backend_handlers::get_item_by_id),
    components(schemas(
        ResponseFediverseInstance,
        ResponsePlatform,
        ResponseCategory,
        ResponseItem,
        ResponseNote,
        ResponseOwnershipStatus,
        ResponsePerson,
        ResponsePersonHandle,
    ))
)]
pub struct ApiDoc;

pub fn create_openapi_definition() -> utoipa::openapi::OpenApi {
    let mut api_doc = ApiDoc::openapi();
    api_doc.info.description = Some("Djundjila's Den Manadjer".to_string());
    api_doc
}

pub fn create_app() -> Router {
    Router::new().merge(
        SwaggerUi::new("/swagger-ui").url("/api-docs/openapi.json", create_openapi_definition()),
    )
}
