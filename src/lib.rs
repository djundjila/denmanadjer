pub fn add(left: usize, right: usize) -> usize {
    left + right
}

pub mod backend_api;
pub mod backend_handlers;
pub mod messages;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
